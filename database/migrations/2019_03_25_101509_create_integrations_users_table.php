<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIntegrationsUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('integrations_users', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->string('user_name'); // The name of the CRM account owner
            $table->string('user_email'); // The email address of the CRM account owner
            $table->string('type'); // =crm
            $table->string('provider'); // The CRM name
            $table->string('provider_id'); // The CRM identifier
            $table->text('access_token');
            $table->text('refresh_token')->nullable();
            $table->dateTime('expires_in')->nullable();
            $table->json('meta')->nullable(); // Additional data
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('integrations_users');
    }
}

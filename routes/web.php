<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group(['prefix'=> 'integrations', 'middleware'=> ['web']], function() {
    # Auth
    Route::get('/auth/{service}', 'CrmAuthController@redirect');
    Route::get('/auth/{service}/callback', 'CrmAuthController@callback');
});

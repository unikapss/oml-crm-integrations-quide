<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
    'namespace' => 'Api',
    'prefix'=> 'api/integrations'
], function() {
    //Dummy routes
    Route::post('/crm/step1', 'CrmController@step1');
    Route::post('/crm/step2', 'CrmController@step2');
});

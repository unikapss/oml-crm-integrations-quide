<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IntegrationsUser extends Model
{
    protected $table = 'integrations_users';

    protected $guarded = [];

    protected $dates = [
        'expires_in'
    ];

    protected $casts = [
        'meta' => 'json'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function crmIntegrations()
    {
        return $this->hasMany(CrmIntegration::class, 'integration_user_id');
    }
}

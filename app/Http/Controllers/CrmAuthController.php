<?php

namespace App\Http\Controllers;

use App\IntegrationsUser;
use App\Jobs\RefreshCrmToken;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;

class CrmAuthController extends Controller
{
    /**
     *  Redirect the user to the social service authentication page.
     *
     * @param $provider
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function redirect($provider)
    {
        //Using socialite is optional
        $scopes = [];

        return Socialite::driver($provider)->scopes($scopes)->redirect();
    }

    /**
     * Obtain the user information from the social service.
     *
     * @param $provider
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function callback($provider)
    {
        $socialUser = Socialite::driver($provider)->user();

        $user = $this->getIntegrationUserIfExists($provider, $socialUser->id);

        if ($user) {
            if ($user->expires_in->isPast()) {
                dispatch(new RefreshCrmToken($user));
            }
            return redirect('/integrations/crm');
        } else {
            $this->createIntegrationUser($provider, $socialUser);

            return redirect('/');
        }
    }

    /**
     * @param $provider
     * @param $provider_id
     * @return mixed
     */
    protected function getIntegrationUserIfExists($provider, $provider_id)
    {
        $query = [
            'user_id' => Auth::id(),
            ['provider', '=', $provider],
            ['provider_id', '=', $provider_id]
        ];

        return IntegrationsUser::where($query)->first();
    }

    /**
     * @param $provider
     * @param $socialUser
     * @return mixed
     */
    protected function createIntegrationUser($provider, $socialUser)
    {
        $expires_in = Carbon::now()->addSeconds($socialUser->accessTokenResponseBody['expires_in_sec']);

        $user_data = [
            'user_id' => Auth::id(),
            'user_name' => $socialUser->name,
            'user_email' => $socialUser->email,
            'provider' => $provider,
            'type' => 'crm',
            'provider_id' => $socialUser->id,
            'access_token' => $socialUser->token,
            'refresh_token' => $socialUser->refreshToken,
            'expires_in' => $expires_in,
            'meta' => Arr::get($socialUser, 'users.0')
        ];

        return IntegrationsUser::create($user_data);
    }
}

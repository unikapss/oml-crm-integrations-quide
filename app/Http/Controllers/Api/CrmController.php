<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\IntegrationsUser;
use GuzzleHttp\Client;
use Illuminate\Http\Response;

class CrmController extends Controller
{
    /**
     * Get the first level data:
     *
     */
    public function step1()
    {
        $user = IntegrationsUser::find(request('integration_user_id'));

        if (!$user) {
            // Handle non
            return response()->json(
                [
                    'success' => false,
                    'message' => 'This'
                ],
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }

        // Get data
        $data = [];

        return response()->json($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     */
    public function step2()
    {
        $user = IntegrationsUser::find(request('integration_user_id'));

        if (!$user) {
            // Handle non
            return response()->json(
                [
                    'success' => false,
                    'message' => 'This'
                ],
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }

        // Get data
        $data = [];

        return response()->json($data);
    }

}

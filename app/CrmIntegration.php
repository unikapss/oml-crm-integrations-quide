<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CrmIntegration extends Model
{
    protected $guarded = [];

    protected $casts = [
        'sources' => 'json',
        'settings' => 'json',
        'status' => 'json',
    ];

    /**
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function provider()
    {
        return $this->belongsTo(IntegrationsUser::class, 'integration_user_id');
    }
}

integrations_users

| Field         | Description                                |
| ------------- | ------------------------------------------ |
| user_id       |                                            |
| user_name     | The name of the CRM account owner          |
| user_email    | The email address of the CRM account owner |
| type          | ='crm'                                     |
| provider      | The CRM name                               |
| provider_id   | The CRM account identifier                 |
| access_token  | The CRM account owner access token         |
| refresh_token | The CRM account owner refresh token        |
| expires_in    | Stored as datetime                         |
| meta          | Any additional data                        |



crm_integrations

| Field               | Description                                |
| ------------------- | ------------------------------------------ |
| user_id             |                                            |
| integration_user_id | refer to the id in integrations_users      |
| settings            | CRM Configuration *                        |



*CRM Configuration 

An array of objects that define the CRM configuration, every object has two fields: id and name

example:

```json
{
     "pipeline": {
        "id": 1,
        "name": "Pipeline"
    },
    "stage": {
        "id": 3,
        "name": "Prospect Qualified"
    }
}
```


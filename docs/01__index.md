#Introduction

CRM integration allow user to automatically export Leads from OhMyLead to third party CRM  like Pipedrive, Zoho crm, Hugspot. Take a quick look here for pipedrive example <https://intercom.help/ohmylead-0328294f6b39/integrations-crm/pipedrive-integration>

You can also create an integration yourself here to see how it works <https://app.ohmylead.com/integrations/crm>



In generation the integration has 5 steps:

1. Connect

   We use OAuth 2 to connect with any third  party Application whenever it's available, but any other way is allowed.
   We use Laravel socialite <https://laravel.com/docs/5.8/socialite> but not limited too.

   See 

2. Store token in integrations_users table

   A migration is  available for you in the sample code

3. Exchange access token (If needed):

   Some Providers have two types of tokens: short lived token and long lived token, if it's the case for the CRM you are about to integration, change the short lived token for long lived token since we will use it on the background to send leads.

4. create OML-- CRM integration configuration 
   Basically we need a controller to retrieve the CRM settings, for example 

   * Zoho CRM have stages so we have a method that retrieve the user stages list 
   * Pipedrive have pipelines and stages take a look at Api\CrmController

   

5. Store Integration

   After Selecting the CRM settings we save it in crm_integrations table as json: 

   example #1:

   ```json
   {
       "stage": {
           "name": "Needs Analysis",
           "id": "2923526000000006803"
       }
   }
   ```

   Example #2 

   ```json
   {
       "pipeline": {
           "name": "Pipeline",
           "id": 1
       },
       "stage": {
           "name": "Lead In",
           "id": 1
       }
   }
   ```

   

   



#Install sample code

* Clone the repository <https://gitlab.com/unikapss/oml-crm-integrations-quide>
* update your database in .env 
* Migrate database:  `php artisan migrate`
* To quickly get started you can check the sample files

